<?php

/**
 * @file
 * EBT Tabs module file.
 */

use Drupal\Core\Routing\RouteMatchInterface;

 /**
 * Implements hook_help().
 */
function ebt_tabs_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the ebt_tabs module.
    case 'help.page.ebt_tabs':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Extra Block Types: Tabs module create a new block type for tabs. Tab content can be blocks, pages, Views and formatted text. Tabs are based on jQuery UI Tabs plugin and have various settings for the jquery plugin.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 */
function ebt_tabs_field_widget_entity_reference_paragraphs_form_alter(&$element, &$form_state, $context) {
  if ($element['#paragraph_type'] != 'ebt_tab') {
    return;
  }

  $options = [
    'text' => 'field_ebt_tab_text',
    'page' => 'field_ebt_tab_page',
    'block' => 'field_ebt_tab_block',
    'views' => 'field_ebt_tab_views',
  ];
  foreach ($options as $option => $field_name) {
    _ebt_tabs_paragraph_field_state($element, $field_name, 'visible', [
      'value' => $option,
    ]);
  }
}

/**
 * Auxiliar function to add state to element.
 */
function _ebt_tabs_paragraph_field_state(&$element, $field_name, $state_key, array $conditions) {
  if (!isset($element['subform'][$field_name])) {
    return;
  }

  $subform = &$element['subform'];
  $parents = $subform['#parents'];
  $parents[] = 'field_ebt_tab_content';

  $field_id = array_shift($parents);
  $field_id .= '[' . implode('][', $parents) . ']';
  $subform[$field_name]['#states'][$state_key][':input[name="' . $field_id . '"]'] = $conditions;
}
